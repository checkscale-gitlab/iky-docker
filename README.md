<div align="center">
    <a href="https://twitter.com/intent/follow?screen_name=kennbroorg">
        <img alt="follow on Twitter" src="https://img.shields.io/twitter/follow/kennbroorg.svg?label=follow%20%40kennbroorg&style=social">
    </a>
</div>

---

<div align="center">
    <a href="https://gitlab.com/kennbroorg/iky-docker/blob/master/README.es.md">
	<img alt="README Espanol" src="https://img.shields.io/badge/README-Espa%C3%B1ol-orange.svg">
    </a>
</div>

---

<div align="center">
    <img alt="Logo" src="https://kennbroorg.gitlab.io/ikyweb/assets/img/Logo-Circular-Docker.png">
</div>

---

# iKy Docker

## Description
Project iKy is a tool that collects information from an email and shows results in a nice visual interface.

Visit the Gitlab Page of the [Project](https://kennbroorg.gitlab.io/ikyweb/)

[![Video Demo](https://kennbroorg.gitlab.io/ikyweb/assets/img/iKy-01.png)](https://vimeo.com/326114716 "Video Demo - Click to Watch!") 

[Video Demo](https://vimeo.com/326114716 "Video Demo - Click to Watch!")

## Docker
Docker must be installed.

### Build image
Build image from dockerfile
```shell
docker build  -t "iky:dockerfile" .
```

### Run container 
Run container from image exposing ports
```shell
docker run -ti --name iky -p 4200:4200 -p 5000:5000 iky:dockerfile
```
Wait a moment after the services wake up (Two minutes approximately)

## Browser
Open the browser in this [url](http://127.0.0.1:4200) 

## Config API Keys
Once the application is loaded in the browser, you should go to the Api Keys option and load the values of the APIs that are needed.

- Fullcontact: Generate the APIs from [here](https://support.fullcontact.com/hc/en-us/articles/115003415888-Getting-Started-FullContact-v2-APIs)
- Twitter: Generate the APIs from [here](https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens.html)
- Linkedin: Only the user and password of your account must be loaded

## Other stuff about Dcoker

### View logs inside the container
To see the logs
```shell
docker exec -i -t iky tail /opt/log/redis.log # Redis LOG
docker exec -i -t iky tail /opt/log/celery.log # Celery LOG
docker exec -i -t iky tail /opt/log/app.log # App LOG
docker exec -i -t iky tail /opt/log/frontend.log # Frontend LOG
```

### Get bash inside the container
To open bash terminal inside the container
```shell
docker exec -i -t iky /bin/bash
```

### View container
To see containers running or stopped
```shell
docker container ps -a
```

### Remove container
To remove container
```shell
docker container rm -f <CONTAINER-ID> 
```
